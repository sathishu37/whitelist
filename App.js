/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import Theme from './Assets/Theme';

class App extends Component{
  render() {
    return (
      <View style = {styles.container}>
        <Text style = {[Theme.primeColor,styles.headerTxt]}>Welcome Sathish Kumar</Text>
        <Text style = {Theme.subHeaderTxt}>Let us create a file called quote1.txt using echo command</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container:{
    flex:1,
    backgroundColor:'#FFF',
    alignItems:'center',
    justifyContent:'center'
  },
  headerTxt:{
    fontSize:16,
    fontWeight:'bold',    
  },
  subHeaderTxt:{
    fontSize:12,
    fontWeight:'bold',    
    marginTop:20
  }
});

export default App;
